<?php

namespace ChristophBerger\Component\SpreadsheetDataUploader\Administrator\View\Uploader;

// Hilfklasse laden
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Form\Form as JForm;
use Joomla\CMS\Factory as JFactory;
use Joomla\CMS\Toolbar\ToolbarHelper as JToolBarHelper;
use Joomla\CMS\HTML\HTMLHelper as JHTML;
use Joomla\CMS\Language\LanguageHelper as JLanguageHelper;
use Joomla\CMS\Language\Text as JText;
use Joomla\Registry\Registry as JRegistry;
use Joomla\CMS\Uri\Uri as JUri;
use Joomla\CMS\Table\Table as JTable;

use ChristophBerger\Component\SpreadsheetDataUploader\Administrator\Helper\BasicHelper as Helper;

// no direct access
defined('_JEXEC') or die('Restricted access');

class HtmlView extends BaseHtmlView {

    protected $form;

    public function display ($tpl = NULL) {

        // Get the Version Number
        $xml = file_get_contents(JPATH_COMPONENT_ADMINISTRATOR . '/spreadsheetdatauploader.xml');
        preg_match('#<version>(\d+(\.\d+(\.\d+)?)?)</version>#', $xml, $Version);
        $this->Version = $Version[1];

        JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/src/Tables/');

        $document = JFactory::getDocument();
        $document->addStyleSheet(JURI::root(true) . '/media/com_spreadsheetdatauploader/css/style.css');
        $document->addStyleSheet(JURI::root(true) . '/media/com_spreadsheetdatauploader/css/bootstrap-fileupload.min.css');
        $document->addStyleSheet(JURI::root(true) . '/media/com_spreadsheetdatauploader/css/bootstrap-select.min.css');
        $document->addScript(JURI::root(true) . '/media/com_spreadsheetdatauploader/js/bootstrap-fileupload.min.js');
        $document->addScript(JURI::root(true) . '/media/com_spreadsheetdatauploader/js/bootstrap-select.min.js');
        $document->addScript(JURI::root(true) . '/media/com_spreadsheetdatauploader/js/date.js');
        $document->addScript(JURI::root(true) . '/media/com_spreadsheetdatauploader/js/jquery.datePicker.js');
        $document->addStyleSheet(JURI::root(true) . '/media/com_spreadsheetdatauploader/css/jquery.datepicker.css');

        $lang = JFactory::getLanguage();
        $lang->load('com_content', JPATH_ADMINISTRATOR);
        $lang->load('com_config', JPATH_ADMINISTRATOR);

        JToolBarHelper::title(JText::_('CU_UPLOAD_ARTICLES'), 'generic.png');
        
        $db = JFactory::getDBO();
        $helper = new Helper;
        $lists = array();

        $query = $db->getQuery(true);
        $query->select($db->quoteName('params'))
            ->from('#__spreadsheetdatauploader')
            ->where($db->quoteName('component') . ' = ' . $db->quote('content'))
            ->where($db->quoteName('active') . ' = ' . $db->quote(1));
        $db->setQuery($query);
        $row = $db->loadResult();
        $params = new JRegistry();
        $params->loadString($row);

        $this->form = JForm::getInstance('cuCoreParams', JPATH_COMPONENT_ADMINISTRATOR . '/src/Model/forms/article.xml');
        $this->form->addFieldPath(JPATH_COMPONENT_ADMINISTRATOR . '/src/Model/fields/');
        $this->form->bind($params);

        $id = $helper->getCurrentConfigID();
        $config = $helper->getConfig($id);
        $lists['configid'] = $helper->getConfigs($id);

        if (!isset($config['fulltext'])) {
            $config['fulltext'] = '';
        }

        $lists['removetags'] = JHTML::_('select.booleanlist', 'removetags', 'class="inputbox"', $config['removetags']);

        $state[] = JHTML::_('select.option', '0', JText::_('JNO'));
        $state[] = JHTML::_('select.option', '1', JText::_('JYES'));
        $lists['catstate'] = JHTML::_('select.radiolist', $state, 'catstate', 'class="inputbox"', 'value', 'text', $config['catstate'] );
        $lists['state'] = JHTML::_('select.radiolist', $state, 'state', 'class="inputbox" onchange="enableField(\'state\',\'state_col\');" ', 'value', 'text', @$config['state'] );
        $lists['featured'] = JHTML::_('select.radiolist', $state, 'featured', 'class="inputbox" onchange="enableField(\'frontpage\',\'frontpage_col\');" ', 'value', 'text', @$config['featured'] );

        
        $languages = JLanguageHelper::getLanguages('lang_code');

        $this->languages = $languages;
        $this->config = $config;
        $this->params = $params;
        $this->lists = $lists;

        parent::display($tpl);
    }
}
